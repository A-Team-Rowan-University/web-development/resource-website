#!/bin/bash

GIT_TAG="$(git describe --tags $(git rev-list --tags --max-count=1))"
GIT_COMMIT="$(git log -1 --pretty=format:'%h on %cd' --date=short)"

export GIT_VERSION="$GIT_TAG ($GIT_COMMIT)"

docker-compose build

if [ "$1" = run ]; then
  docker-compose up -d
fi
