from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from .models import *
from trainings.models import UserTraining, Training


@login_required(login_url='/login/google-oauth2/')
def view_index(request):
    user_trainings = UserTraining.objects.filter(trainee=request.user)
    all_machines = Machine.objects.all()
    user_machines = []

    for machine in Machine.objects.all():
        if machine.can_operate(request.user):
            user_machines.append(machine)

    context = {
        'all_machines': all_machines,
        'user_machines': user_machines
    }
    return render(request, 'lockout/index.html', context)
