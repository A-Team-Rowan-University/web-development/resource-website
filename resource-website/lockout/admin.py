from django.contrib import admin
from .models import *


class MachineAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_per_page = 25


class MachineWorkEntryAdmin(admin.ModelAdmin):
    list_display = ('machine', 'created', 'resolved')
    list_per_page = 25


class MachineUseEntryAdmin(admin.ModelAdmin):
    list_display = ('machine', 'user', 'start', 'end')
    list_per_page = 25


admin.site.register(Machine, MachineAdmin)
admin.site.register(MachineWorkEntry, MachineWorkEntryAdmin)
admin.site.register(MachineUseEntry, MachineUseEntryAdmin)
