# Generated by Django 3.1.5 on 2021-08-15 23:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0026_auto_20210103_1340'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='altium_footprint',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='part',
            name='altium_symbol',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
    ]
