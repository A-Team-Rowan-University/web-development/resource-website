# Generated by Django 3.0.2 on 2020-08-30 05:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0014_auto_20200828_2204'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'permissions': (('fill_orders', 'Can fill orders'),)},
        ),
        migrations.AlterModelOptions(
            name='part',
            options={'permissions': (('use_inventory', 'Can use inventory'), ('edit_parts', 'Can edit parts'))},
        ),
    ]
