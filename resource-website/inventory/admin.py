from django.contrib import admin
from .models import *


class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('name', 'website_url')
    search_fields = ('name',)
    list_per_page = 25


class PropertyInline(admin.TabularInline):
    model = Property
    extra = 0


class PartInline(admin.TabularInline):
    fields = ('name', 'manufacturer', 'manufacturer_part_number')
    readonly_fields = ('name', 'manufacturer', 'manufacturer_part_number')
    model = Part
    extra = 0


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'part_name_template')
    list_per_page = 25
    autocomplete_fields = ('parent',)
    search_fields = ('name', 'parent')
    ordering = ('parent', 'name',)
    inlines = (PropertyInline, PartInline, )


class SupplierAdmin(admin.ModelAdmin):
    list_display = ('name', 'website_url')
    list_per_page = 25
    search_fields = ('name',)


class PartPropertyInline(admin.TabularInline):
    model = PartProperty
    extra = 0

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'property' and 'object_id' in request.resolver_match.kwargs:
            part = Part.objects.get(pk=request.resolver_match.kwargs['object_id'])
            query = Property.objects.filter(part.category.properties_q())
            kwargs['queryset'] = query

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class PartSupplierInline(admin.StackedInline):
    model = PartSupplier
    extra = 0
    autocomplete_fields = ('supplier',)


class PartLocationInline(admin.TabularInline):
    model = PartLocation
    extra = 0
    autocomplete_fields = ('location', 'part')


class PartAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'category', 'manufacturer')
    list_per_page = 25
    list_filter = ('manufacturer', 'category')
    autocomplete_fields = ('manufacturer', 'category', )
    inlines = (PartPropertyInline, PartSupplierInline, PartLocationInline)
    search_fields = ('id', 'name_override', 'category__name', 'description', 'manufacturer__name', 'manufacturer_part_number', 'partproperty__value')
    ordering = ('id',)


class PartSupplierAdmin(admin.ModelAdmin):
    list_display = ('part', 'supplier', 'supplier_part_number', 'supplier_part_url', 'price')
    list_per_page = 25


class PropertyAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'unit')
    list_per_page = 25
    search_fields = ('name', 'category')


class PartPropertyAdmin(admin.ModelAdmin):
    list_display = ('part', 'property', 'value')
    list_per_page = 25


class LocationAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'non_local')
    list_per_page = 25
    autocomplete_fields = ('parent',)
    search_fields = ('parent', 'name',)
    ordering = ('parent', 'name',)
    inlines = (PartLocationInline, )


class PartLocationAdmin(admin.ModelAdmin):
    list_display = ('location', 'part', 'quantity', 'barcode', 'primary_location')
    list_per_page = 25
    search_fields = ('location', 'part', 'barcode')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('user', 'status')
    list_filter = ('status', )
    list_per_page = 25


class PartOrderFulfillmentInline(admin.TabularInline):
    model = PartOrderFulfillment
    extra = 0
    autocomplete_fields = ('part_location',)


class PartOrderAdmin(admin.ModelAdmin):
    list_display = ('order', 'part', 'quantity', 'fulfilled_quantity')
    list_per_page = 25
    inlines = (PartOrderFulfillmentInline, )


class OrderStateChangesAdmin(admin.ModelAdmin):
    list_display = ('order', 'last_state', 'next_state', 'timestamp', 'changed_by')
    list_per_page = 25


class RequestAdmin(admin.ModelAdmin):
    list_display = ('user', 'status', 'request', 'notes')
    list_filter = ('status', )
    list_per_page = 25


admin.site.register(Manufacturer, ManufacturerAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Supplier, SupplierAdmin)
admin.site.register(Part, PartAdmin)
admin.site.register(PartSupplier, PartSupplierAdmin)
admin.site.register(Property, PropertyAdmin)
admin.site.register(PartProperty, PartPropertyAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(PartLocation, PartLocationAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(PartOrder, PartOrderAdmin)
admin.site.register(OrderStateChanges, OrderStateChangesAdmin)
admin.site.register(Request, RequestAdmin)


