import argparse
import csv
from django.core.management.base import BaseCommand

from inventory.models import Order, PartOrder, Part
from user_management.models import CustomUser


class Command(BaseCommand):
    help = (
        "Imports inventory orders from a csv file in the format \n"
        "path | non local\n"
    )

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs=1, type=argparse.FileType('rt'))

    def handle(self, *args, **options):
        file = options['csv_file'][0]
        reader = csv.reader(file)
        for i, row in enumerate(reader):
            print("Importing {: 6}".format(i), end='')

            status = row[0]
            username = row[1]
            parts = row[2]

            user = CustomUser.objects.get(username=username)

            order = Order(status=status, user=user)
            order.save()
            for part in parts.split('|'):
                part_no, qty = part.split('=')
                part = Part.objects.get(manufacturer_part_number=part_no)
                po = PartOrder(part=part, quantity=qty, order=order)
                po.save()

            print('\r', end='')
