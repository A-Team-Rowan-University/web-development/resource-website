import urllib.request
from io import BytesIO
from urllib.parse import urlencode, unquote
from urllib.request import urlopen

import json

import itertools

from django.conf import settings
from django.contrib.auth.decorators import login_required, permission_required
from django.core.paginator import Paginator
from django.db.models import OuterRef, Subquery, Q, F

from django.http import HttpResponse

from django.shortcuts import render, redirect

import uuid
from pylibdmtx import pylibdmtx
from PIL import Image

from natsort import realsorted

from .metric import parse_metric, render_metric, clean_metric
from .forms import *
from .models import *

from django.utils import timezone

from django.contrib.auth.decorators import login_required, user_passes_test, permission_required


def num_of_cart_items(user):
    cart = Order.objects.get(user=user, status=OrderStatus.CART)
    return cart.parts.count()


def iter_chunk(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
def view_index(request):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()
    form = HomepageSearchForm(request.POST or None)
    categories = []

    for category in Category.objects.filter(parent=None):
        temp = Category.objects.filter(parent=category)
        categories.append((category, temp))

    context = {
        'form': form,
        'categories': categories,
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/index.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
def view_category(request, category_id, page=None, order='id'):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()

    order = unquote(order)

    if request.method == 'POST':
        if request.POST['action'] == 'cart':
            part = Part.objects.get(pk=int(request.POST['part_id']))
            if PartOrder.objects.filter(order=cart, part=part):
                part_order = PartOrder.objects.get(order=cart, part=part)
                part_order.quantity += int(request.POST['qty'])
                part_order.save()
            else:
                part_order = PartOrder(part=part, quantity=request.POST['qty'], order=cart)
                part_order.save()

    main_category = Category.objects.get(pk=category_id)

    category_path = [main_category]
    category_path += (main_category.get_all_parentcategories())
    category_path.reverse()

    subcategories = []
    for category in Category.objects.filter(parent=main_category):
        temp = Category.objects.filter(parent=category)
        subcategories.append((category, temp))

    no_child_categories = request.GET.get('no_child_categories')

    if no_child_categories:
        parts = Part.objects.filter(category=main_category)
    else:
        parts = Part.objects.filter(main_category.parts_q())

    in_stock = 'in_stock' in request.GET

    if in_stock:
        part_locations_with_stock = PartLocation.objects.filter(quantity__gt=0)
        parts = parts.filter(partlocation__in=list(part_locations_with_stock))

    manufacturer_filter = request.GET.getlist('manufacturer')
    if len(manufacturer_filter) > 0:
        manufacturer_q = Q(manufacturer__name__in=manufacturer_filter)
        if '__none__' in manufacturer_filter:
            manufacturer_q = manufacturer_q | Q(manufacturer__isnull=True)

        parts = parts.filter(manufacturer_q)

    status_filter = request.GET.getlist('status')
    if len(status_filter) > 0:
        parts = parts.filter(status__in=status_filter)

    for property_name in request.GET:
        if property_name.startswith('property_'):
            values = request.GET.getlist(property_name)
            property_name = property_name[len('property_'):]
            property = Property.objects.filter(main_category.properties_q()).get(name=property_name)

            q = Q(property_value__in=values)

            if '__none__' in values:
                q = q | Q(property_value__isnull=True)

            part_property_query = PartProperty.objects.filter(property=property, part=OuterRef('pk'))
            parts = parts.annotate(property_value=Subquery(part_property_query.values('value'))).filter(q)

    # Do this if so that part of the URL does not go straight to the db
    # might be a security problem
    if order == 'id':
        parts = parts.order_by('id')
    elif order == '-id':
        parts = parts.order_by('-id')
    elif order == 'mfg_partno':
        parts = parts.order_by('manufacturer_part_number')
    elif order == '-mfg_partno':
        parts = parts.order_by('-manufacturer_part_number')
    elif order == 'mfg':
        parts = parts.order_by('manufacturer__name')
    elif order == '-mfg':
        parts = parts.order_by('-manufacturer__name')
    elif order == 'status':
        parts = parts.order_by('status')
    elif order == '-status':
        parts = parts.order_by('-status')
    elif order.startswith('property_'):
        property_name = order[len('property_'):]
        property = Property.objects.filter(name=property_name).first()
        if property is not None:
            part_property_query = PartProperty.objects.filter(property=property, part=OuterRef('pk'))
            parts = parts.annotate(order_property=Subquery(part_property_query.values('value_sort'))).order_by(
                'order_property')
    elif order.startswith('-property_'):
        property_name = order[len('-property_'):]
        property = Property.objects.filter(name=property_name)[:1]
        if property is not None:
            part_property_query = PartProperty.objects.filter(property=property, part=OuterRef('pk'))
            parts = parts.annotate(order_property=Subquery(part_property_query.values('value_sort'))).order_by(
                '-order_property')

    parts_pages = Paginator(parts, 25)
    parts_page_number = page if page is not None else 1
    parts_page = parts_pages.get_page(parts_page_number)

    manufacturer_values = realsorted(set(parts.values_list('manufacturer__name', flat=True)))
    manufacturer_values = list(map(
        lambda v: (v, v, v in manufacturer_filter or (v is None and '__none__' in manufacturer_filter)),
        manufacturer_values
    ))

    status_values = realsorted(set(parts.values_list('status', flat=True)))
    status_values = list(map(
        lambda v: (v, dict(PartStatus.choices)[v], v in status_filter),
        status_values
    ))

    properties = []

    for property in Property.objects.filter(main_category.properties_q()):
        part_property_query = PartProperty.objects.filter(property=property, part=OuterRef('pk'))
        all_values = parts \
            .values(
            property_value=Subquery(part_property_query.values('value')),
            property_value_sort=Subquery(part_property_query.values('value_sort'))
        ) \
            .values_list('property_value_sort', 'property_value')

        values = map(
            lambda v: v[1], realsorted(
                dict(all_values).items(),
                key=lambda v: v[1] if v[1] is not None else ''
            )
        )

        filter_values = request.GET.getlist('property_' + property.name)
        values = list(map(lambda v: (v, property.render(v) if v is not None else None,
                                     v in filter_values or (v is None and '__none__' in filter_values)), values))

        properties.append((property, values))

    parts_and_properties = []
    for part in parts_page:
        property_values = []
        for property, values in properties:
            part_property = PartProperty.objects.filter(part=part, property=property).first()
            if part_property is not None:
                property_values.append((property.name, part_property.rendered()))
            else:
                property_values.append((property.name, ''))
        parts_and_properties.append((part, property_values))

    part_numbers = '\n'.join(map(lambda p: p.manufacturer_part_number, parts_page))
    import_parts_form = SupplierImportForm(initial={'category': main_category, 'part_numbers': part_numbers})
    part_locations_form = PartLocationForm(initial={'part_numbers': part_numbers})

    context = {
        'main_category': main_category,
        'subcategories': subcategories,
        'properties': properties,
        'manufacturer_values': manufacturer_values,
        'status_values': status_values,
        'parts_pages': parts_pages,
        'parts_page': parts_page,
        'parts_and_properties': parts_and_properties,
        'order': order,
        'category_path': category_path,
        'in_stock': in_stock,
        'no_child_categories': no_child_categories,
        'params': urlencode(request.GET),
        'import_parts_form': import_parts_form,
        'part_locations_form': part_locations_form,
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/category_details.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.edit_parts', raise_exception=True)
def view_part_locations(request, category_id):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()

    if request.method == 'POST':
        if request.POST['action'] == "add":
            part = Part.objects.get(pk=request.POST['part'])
            location = Location.objects.first()
            part_location = PartLocation(part=part, location=location, quantity=0, primary_location=False)
            part_location.save()
        elif request.POST['action'] == 'remove':
            part_location = PartLocation(pk=request.POST['part_location'])
            part_location.delete()
        elif request.POST['action'] == 'update':
            part_location = PartLocation.objects.get(pk=request.POST['part_location'])
            location = Location.objects.get(pk=request.POST['location'])
            quantity = request.POST['quantity']
            barcode = request.POST['barcode']
            primary = 'primary' in request.POST

            part_location.location = location
            part_location.quantity = quantity
            part_location.barcode = barcode
            part_location.primary_location = primary
            part_location.save()

    main_category = Category.objects.get(pk=category_id)

    if len(request.GET) > 0:
        form = PartLocationForm(request.GET)
        if form.is_valid():
            part_numbers = list(map(lambda p: p.strip(), form.cleaned_data['part_numbers'].split('\n')))
            parts = Part.objects.filter(manufacturer_part_number__in=part_numbers)
        else:
            parts = Part.objects.filter(main_category.parts_q()).order_by('id')
    else:
        parts = Part.objects.filter(main_category.parts_q()).order_by('id')

    parts_pages = Paginator(parts, 25)

    parts_page_number = request.GET.get('page')
    parts_page = parts_pages.get_page(parts_page_number)

    parts_and_part_locations = []
    for part in parts_page:
        part_locations = []
        for part_location in PartLocation.objects.filter(part=part):
            part_locations.append(part_location)
        parts_and_part_locations.append((part, part_locations))

    locations = []

    for location in Location.objects.all():
        path = [location] + location.parents()
        path = list(reversed(path))

        path_str = '/'.join(map(lambda l: l.name, path))
        path_ids = list(map(lambda l: l.id, path))

        locations.append((location.id, location.name, path_str, path_ids))

    locations = sorted(locations, key=lambda location: location[2])

    context = {
        'main_category': main_category,
        'parts_pages': parts_pages,
        'parts_page': parts_page,
        'parts_and_part_locations': parts_and_part_locations,
        'locations': locations,
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/part_locations.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
def view_part(request, part_id):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()
    part = Part.objects.get(pk=part_id)
    category_path = []
    category_path.append(part.category)
    category_path += (part.category.get_all_parentcategories())

    category_path.reverse()

    properties = PartProperty.objects.filter(part=part)

    altium_symbol = False if not part.altium_symbol else True
    altium_footprint = False if not part.altium_footprint else True

    if request.method == 'POST':
        part = Part.objects.get(pk=int(request.POST['part_id']))
        if PartOrder.objects.filter(order=cart, part=part):
            part_order = PartOrder.objects.get(order=cart, part=part)
            part_order.quantity += int(request.POST['qty'])
            part_order.save()
        else:
            part_order = PartOrder(part=part, quantity=request.POST['qty'], order=cart)
            part_order.save()

    context = {
        'part': part,
        'altium_symbol': altium_symbol,
        'altium_footprint': altium_footprint,
        'category_path': category_path,
        'properties': properties,
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/part_details.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
def view_cart(request):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()
    parts = PartOrder.objects.filter(order=cart)
    if request.method == 'POST':
        action = request.POST['submit']
        if action == 'Update':
            cart_part_id = request.POST['cart_part_id']
            part = PartOrder.objects.get(pk=cart_part_id)
            part.quantity = request.POST['qty']
            part.save()
        elif action == 'Delete':
            cart_part_id = request.POST['cart_part_id']
            part = PartOrder.objects.get(pk=cart_part_id)
            part.delete()
        elif action == 'Order':
            cart.status = OrderStatus.SUBMITTED
            cart.save()
            order_state = OrderStateChanges(order=cart, last_state=OrderStatus.CART, next_state=OrderStatus.SUBMITTED,
                                            timestamp=timezone.now(), changed_by=request.user)
            order_state.save()
            return redirect(view_previous_orders)
    if len(parts) == 0:
        empty = True
    else:
        empty = False

    context = {
        'order': cart,
        'parts': parts,
        'empty': empty,
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/cart.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
@permission_required('inventory.digikey_auth', raise_exception=True)
def view_digikey_auth(request):
    auth_code = request.GET.get('code')
    error = request.GET.get('error')

    if auth_code is not None:
        body_parameters = {
            'code': auth_code,
            'client_id': settings.DIGIKEY_API_CLIENT_ID,
            'client_secret': settings.DIGIKEY_API_CLIENT_SECRET,
            'redirect_uri': settings.DIGIKEY_API_REDIRECT_URL,
            'grant_type': 'authorization_code',
        }

        body = urlencode(body_parameters)

        url = '{}/v1/oauth2/token'.format(settings.DIGIKEY_API_URL)

        response = urlopen(url, data=body.encode('utf-8'))

        json_response = json.load(response)

        auth = DigiKeyAuth(
            access_token=json_response['access_token'],
            refresh_token=json_response['refresh_token'],
            expired_in=json_response['expires_in'],
            refresh_token_expires_in=json_response['refresh_token_expires_in'],
            timestamp=timezone.now(),
            auto_refreshed=False,
            user=request.user,
        )

        auth.save()

        return redirect(view_index)
    elif error is not None:
        return HttpResponse(error)
    else:
        auth_url = '{}/v1/oauth2/authorize'.format(settings.DIGIKEY_API_URL)

        query_parameters = {
            'response_type': 'code',
            'client_id': settings.DIGIKEY_API_CLIENT_ID,
            'redirect_uri': settings.DIGIKEY_API_REDIRECT_URL,
        }

        query = urlencode(query_parameters)

        url = '{}?{}'.format(auth_url, query)

        return redirect(url)


class DigiKeyPart:
    def __init__(self, manufacturer, manufacturer_part_number, digikey_part_number, digikey_url, price, datasheet,
                 description, properties, existing_part):
        self.manufacturer = manufacturer
        self.manufacturer_part_number = manufacturer_part_number
        self.digikey_part_number = digikey_part_number
        self.digikey_url = digikey_url
        self.price = price
        self.datasheet = datasheet
        self.description = description
        self.properties = properties
        self.existing_part = existing_part


def digikey_part(part_json):
    return DigiKeyPart(
        manufacturer=part_json['Manufacturer']['Value'],
        manufacturer_part_number=part_json['ManufacturerPartNumber'],
        digikey_part_number=part_json['DigiKeyPartNumber'],
        digikey_url=part_json['ProductUrl'],
        price=part_json['UnitPrice'],
        datasheet=part_json['PrimaryDatasheet'],
        description=part_json['DetailedDescription'],
        properties=list(map(lambda p: (p['Parameter'], clean_metric(p['Value']), p['Value']), part_json['Parameters'])),
        existing_part=Part.objects.filter(manufacturer_part_number=part_json['ManufacturerPartNumber']).first()
    )


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.digikey_import', raise_exception=True)
@permission_required('inventory.use_inventory', raise_exception=True)
def view_digikey_lookup(request):
    parts = []
    properties = []
    digikey_properties = []

    form = SupplierImportForm(request.GET)

    if form.is_valid():
        if request.method == "POST" and 'data' in request.POST:
            data = json.loads(request.POST['data'])
            supplier = Supplier.objects.get(name="Digi-Key")
            category = Category.objects.get(pk=request.POST['category'])
            for json_part in data:
                part = Part.objects.filter(manufacturer_part_number=json_part['manufacturer_part_number']).first()
                manufacturer, _ = Manufacturer.objects.get_or_create(name=json_part['manufacturer'])
                if part is None:
                    part = Part(
                        manufacturer=manufacturer,
                        manufacturer_part_number=json_part['manufacturer_part_number'],
                        category=category,
                        datasheet=json_part['datasheet'],
                        description=json_part['description'],
                    )

                part.manufacturer = manufacturer
                part.manufacturer_part_number = json_part['manufacturer_part_number']
                part.category = category
                part.datasheet = json_part['datasheet']
                part.description = json_part['description']
                part.save();

                part_supplier = PartSupplier.objects.filter(part=part, supplier=supplier).first()
                if part_supplier is None:
                    part_supplier = PartSupplier(part=part, supplier=supplier)
                    part_supplier.save()

                part_supplier.supplier_part_number = json_part['digikey_part_number']
                part_supplier.supplier_part_url = json_part['digikey_url']
                part_supplier.price = json_part['price']
                part_supplier.save()

                for id, value in json_part['properties'].items():
                    property = Property.objects.get(pk=id)
                    if value is not None:
                        if property.metric:
                            value = parse_metric(value)
                        part_property = PartProperty.objects.filter(part=part, property=property).first()
                        if part_property is None:
                            part_property = PartProperty(property=property, part=part, value=value)
                            part_property.save()

                        part_property.value = value
                        part_property.save()

            return redirect(view_category, category_id=form.cleaned_data['category'].id)

        auth = DigiKeyAuth.objects.order_by('-id').first()
        if auth is not None:

            refresh_data = {
                'client_id': settings.DIGIKEY_API_CLIENT_ID,
                'client_secret': settings.DIGIKEY_API_CLIENT_SECRET,
                'refresh_token': auth.refresh_token,
                'grant_type': 'refresh_token'
            }

            r = urllib.request.Request(
                '{}/v1/oauth2/token'.format(settings.DIGIKEY_API_URL),
                data=urlencode(refresh_data).encode('utf-8'),
                headers={
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            )

            refresh_resp = urlopen(r)

            json_refresh = json.load(refresh_resp)

            auth = DigiKeyAuth(
                access_token=json_refresh['access_token'],
                refresh_token=json_refresh['refresh_token'],
                expired_in=json_refresh['expires_in'],
                refresh_token_expires_in=json_refresh['refresh_token_expires_in'],
                timestamp=timezone.now(),
                auto_refreshed=True,
                user=request.user,
            )

            auth.save()

            part_numbers = list(map(lambda p: p.strip(), form.cleaned_data['part_numbers'].split('\n')))
            data = {
                'Products': part_numbers
            }
            r = urllib.request.Request(
                '{}/BatchSearch/v3/ProductDetails'.format(settings.DIGIKEY_API_URL),
                data=json.dumps(data).encode('utf-8'),
                headers={
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer {}'.format(auth.access_token),
                    'X-Digikey-Client-Id': settings.DIGIKEY_API_CLIENT_ID,
                },
            )

            resp = urllib.request.urlopen(r)

            json_response = json.load(resp)

            parts = list(map(digikey_part, json_response['ProductDetails']))

            properties = Property.objects.filter(form.cleaned_data['category'].properties_q())

            for part in parts:
                digikey_properties = sorted(
                    list(set(digikey_properties) | set(map(lambda p: p[0], part.properties))))

    else:
        form = SupplierImportForm()

    context = {
        'form': form,
        'parts': parts,
        'properties': properties,
        'digikey_properties': digikey_properties,
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/digikey_lookup.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
def view_previous_orders(request):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()
    orders = Order.objects.filter(user=request.user).exclude(status=OrderStatus.CART)

    order_data = []
    for order in orders:
        state_change = OrderStateChanges.objects.get(order=order, last_state=OrderStatus.CART,
                                                     next_state=OrderStatus.SUBMITTED)
        timestamp = state_change.timestamp
        part_orders = PartOrder.objects.filter(order=order)
        if order.status == OrderStatus.SUBMITTED:
            progress_bar = 25
        elif order.status == OrderStatus.PROCESSING:
            progress_bar = 60
        elif order.status == OrderStatus.COMPLETE:
            progress_bar = 100
        elif order.status == OrderStatus.PICKED_UP:
            progress_bar = 100
        elif order.status == OrderStatus.CANCELLED:
            progress_bar = 0
        elif order.status == OrderStatus.REJECTED:
            progress_bar = 0
        else:
            progress_bar = 0
        order_data.append((order, timestamp, part_orders, progress_bar))

    order_data = list(reversed(sorted(order_data, key=lambda order: order[1])))

    requests = Request.objects.filter(user=request.user).order_by('-id')

    context = {
        'order_data': order_data,
        'requests': requests,
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/previous_orders.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
@permission_required('inventory.generate_barcode_sheets', raise_exception=True)
def view_barcodes(request):
    dpi = 600

    page_width = int(8.5 * dpi)
    page_height = int(11.0 * dpi)

    barcode_size = int(0.3 * dpi)
    barcode_spacing_x = int(0.72 * dpi)
    barcode_spacing_y = int(0.69 * dpi)

    barcode_rows = int(14)
    barcode_cols = int(11)

    barcode_offset_x = int(0.53 * dpi)
    barcode_offset_y = int(0.91 * dpi)

    sheet_img = Image.new('RGB', (page_width, page_height), (255, 255, 255))

    for row in range(0, barcode_rows):
        for col in range(0, barcode_cols):
            id = uuid.uuid1()
            encoded = pylibdmtx.encode('PL:{}'.format(id.hex).encode('UTF-8'))
            barcode_img = Image.frombytes('RGB', (encoded.width, encoded.height), encoded.pixels).resize(
                (barcode_size, barcode_size))
            sheet_img.paste(barcode_img.copy(),
                            (barcode_offset_x + col * barcode_spacing_x, barcode_offset_y + row * barcode_spacing_y))

    bytes = BytesIO()
    sheet_img.save(bytes, "PDF", resolution=dpi)
    response = HttpResponse(bytes.getvalue(), content_type="application/pdf")
    return response


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
@permission_required('inventory.lookup_barcodes')
def view_barcode_lookup(request):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()
    barcode = request.GET.get('barcode')
    part_locations = PartLocation.objects.filter(barcode=barcode)
    context = {
        'barcode': barcode,
        'part_locations': part_locations,
    }
    return render(request, 'inventory/lookup_barcodes.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
@permission_required('inventory.fill_orders', raise_exception=True)
def fill_orders(request):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()

    if request.method == 'POST':
        action = request.POST['action']
        if action == 'picked_up':
            order = Order.objects.get(pk=request.POST['order'])
            order_state_change = OrderStateChanges(order=order, last_state=order.status,
                                                   next_state=OrderStatus.PICKED_UP, timestamp=timezone.now(),
                                                   changed_by=request.user)
            order_state_change.save()
            order.status = OrderStatus.PICKED_UP
            order.save()
        if action == 'reject_request':
            part_request = Request.objects.get(pk=request.POST['request'])
            part_request.status = RequestStatus.REJECTED
            part_request.notes = request.POST['notes']
            part_request.save()
        if action == 'complete_request':
            part_request = Request.objects.get(pk=request.POST['request'])
            part_request.status = RequestStatus.COMPLETE
            part_request.notes = request.POST['notes']
            part_request.save()

    orders = Order.objects.all().exclude(status=OrderStatus.CART)
    in_progress_orders = []
    completed_orders = []
    for order in orders:
        state_change = OrderStateChanges.objects.get(order=order, last_state=OrderStatus.CART,
                                                     next_state=OrderStatus.SUBMITTED)
        timestamp = state_change.timestamp
        part_orders = PartOrder.objects.filter(order=order)
        if order.status == OrderStatus.SUBMITTED or order.status == OrderStatus.PROCESSING:
            in_progress_orders.append((order, timestamp, part_orders))
        else:
            completed_orders.append((order, timestamp, part_orders))

    in_progress_orders = list(reversed(sorted(in_progress_orders, key=lambda order: order[1])))
    completed_orders = list(reversed(sorted(completed_orders, key=lambda order: order[1])))

    orders_pages = Paginator(completed_orders, 25)

    orders_page_number = request.GET.get('page')
    orders_page = orders_pages.get_page(orders_page_number)

    unfilled_requests = Request.objects.filter(status=RequestStatus.SUBMITTED).order_by('-id')
    filled_requests = Request.objects.exclude(status=RequestStatus.SUBMITTED).order_by('-id')

    context = {
        'in_progress_orders': in_progress_orders,
        'orders_page': orders_page,
        'orders_pages': orders_pages,
        'unfilled_requests': unfilled_requests,
        'filled_requests': filled_requests,
        'num_of_cart_items': num_of_cart_items(request.user),
    }
    return render(request, 'inventory/fillorders.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
@permission_required('inventory.fill_orders', raise_exception=True)
def fill_order(request, order_id):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()

    order = Order.objects.get(pk=order_id)
    audio = None
    if request.method == 'POST':
        action = request.POST['action']
        if action == 'process':
            order_state_change = OrderStateChanges(order=order, last_state=order.status,
                                                   next_state=OrderStatus.PROCESSING, timestamp=timezone.now(),
                                                   changed_by=request.user)
            order_state_change.save()
            order.status = OrderStatus.PROCESSING
            order.save()
        elif action == 'unprocess':
            order_state_change = OrderStateChanges(order=order, last_state=order.status,
                                                   next_state=OrderStatus.SUBMITTED, timestamp=timezone.now(),
                                                   changed_by=request.user)
            order_state_change.save()
            order.status = OrderStatus.SUBMITTED
            order.save()
        elif action == 'barcode':
            part_location = PartLocation.objects.filter(barcode=request.POST['barcode']).first()
            if part_location is not None:
                part_order = PartOrder.objects.filter(order=order, part=part_location.part).first()
                if part_order is not None:
                    audio = "scan-success.wav"
                    quantity = min(part_order.quantity - part_order.fulfilled_quantity, part_location.quantity)
                    fulfillment = PartOrderFulfillment.objects.filter(part_order=part_order,
                                                                      part_location=part_location,
                                                                      quantity=quantity).first()
                    if fulfillment is None:
                        fulfillment = PartOrderFulfillment(part_order=part_order, part_location=part_location,
                                                           quantity=quantity)
                    else:
                        fulfillment.quantity = quantity
                    fulfillment.save()
            else:
                audio = "scan-fail.wav"
        elif action == 'add':
            part_order = PartOrder.objects.get(pk=request.POST['part_order'])
            part_location = PartLocation.objects.get(pk=request.POST['part_location'])
            quantity = min(part_order.quantity - part_order.fulfilled_quantity, part_location.quantity)
            fulfillment = PartOrderFulfillment(part_order=part_order, part_location=part_location, quantity=quantity)
            fulfillment.save()
        elif action == 'remove':
            fulfillment = PartOrderFulfillment.objects.get(pk=request.POST['fulfillment'])
            fulfillment.delete()
        elif action == 'edit':
            if len(request.POST['quantity']) > 0:
                fulfillment = PartOrderFulfillment.objects.get(pk=request.POST['fulfillment'])
                fulfillment.quantity = request.POST['quantity']
                fulfillment.save()
        elif action == 'part_location':
            if len(request.POST['quantity']) > 0:
                part_location = PartLocation.objects.get(pk=request.POST['part_location'])
                part_location.quantity = request.POST['quantity']
                part_location.save()
        elif action == 'refill':
            if len(request.POST['refill_part_location']) > 0 and len(request.POST['quantity']) > 0:
                part_location = PartLocation.objects.get(pk=request.POST['part_location'])
                refill_part_location = PartLocation.objects.get(pk=request.POST['refill_part_location'])
                quantity = min(int(request.POST['quantity']), refill_part_location.quantity)
                refill_part_location.quantity -= quantity
                part_location.quantity += quantity
                refill_part_location.save()
                part_location.save()
        elif action == 'complete':

            for part_order in order.partorder_set.all():
                for fulfillment in part_order.partorderfulfillment_set.all():
                    part_location = fulfillment.part_location
                    part_location.quantity -= min(fulfillment.quantity, part_location.quantity)
                    part_location.save()

            order_state_change = OrderStateChanges(order=order, last_state=order.status,
                                                   next_state=OrderStatus.COMPLETE, timestamp=timezone.now(),
                                                   changed_by=request.user)
            order_state_change.save()
            order.status = OrderStatus.COMPLETE
            order.save()

            return redirect(fill_orders)

    part_orders = []

    for part_order in order.partorder_set.all().order_by('fulfilled'):
        part_locations_and_fulfillments = []
        for part_location in part_order.part.partlocation_set.all():
            fulfillment = PartOrderFulfillment.objects.filter(part_location=part_location,
                                                              part_order=part_order).first()
            part_locations_and_fulfillments.append((part_location, fulfillment))

        part_orders.append((part_order, part_locations_and_fulfillments))

    context = {
        'order': order,
        'part_orders': part_orders,
        'num_of_cart_items': num_of_cart_items(request.user),
        'audio': audio,
    }
    print(context)
    return render(request, 'inventory/fillorder.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
def view_submit_request(request):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()

    if request.method == 'POST':
        part_request = Request(
            status=RequestStatus.SUBMITTED,
            user=request.user,
            request=request.POST['request'],
            notes='',
        )
        part_request.save()

        return redirect(view_index)

    context = {
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/request.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
def view_analytics_categories(request):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()

    categories = Category.objects.all()

    order = request.GET.get('order', 'name')

    # TODO: These sorted()s are very bad for perfprmance.
    #  It should be done with .order_by(), but that would get fairly complicated.
    #  This page is not accessed that often, so I'm not too concerned about performance
    #  - TIMH

    if order == 'path':
        categories = sorted(categories, key=lambda c: str(c))
    elif order == '-path':
        categories = sorted(categories, key=lambda c: str(c), reverse=True)
    elif order == 'parts_count':
        categories = sorted(categories, key=lambda c: c.parts_count(), reverse=True)
    elif order == '-parts_count':
        categories = sorted(categories, key=lambda c: c.parts_count())
    elif order == 'locations_count':
        categories = sorted(categories, key=lambda c: c.part_locations_count(), reverse=True)
    elif order == '-locations_count':
        categories = sorted(categories, key=lambda c: c.part_locations_count())
    elif order == 'parts_quantity':
        categories = sorted(categories, key=lambda c: c.parts_quantity(), reverse=True)
    elif order == '-parts_quantity':
        categories = sorted(categories, key=lambda c: c.parts_quantity())
    elif order == 'parts_price':
        categories = sorted(categories, key=lambda c: c.parts_price(), reverse=True)
    elif order == '-parts_price':
        categories = sorted(categories, key=lambda c: c.parts_price())
    elif order == 'parts_ordered':
        categories = sorted(categories, key=lambda c: c.parts_ordered(), reverse=True)
    elif order == '-parts_ordered':
        categories = sorted(categories, key=lambda c: c.parts_ordered())

    context = {
        'categories': categories,
        'order': order,
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/analytics_categories.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('inventory.use_inventory', raise_exception=True)
def view_analytics_parts(request):
    cart = Order.objects.filter(status=OrderStatus.CART, user=request.user).first()
    if cart is None:
        cart = Order(status=OrderStatus.CART, user=request.user)
        cart.save()

    order = request.GET.get('order', 'ordered')

    parts = Part.objects.all()

    if order == 'name':
        parts = sorted(parts, key=lambda p: p.name)
    elif order == 'name':
        parts = sorted(parts, key=lambda p: p.name, reverse=True)
    elif order == 'ordered':
        parts = sorted(parts, key=lambda p: p.quantity_ordered(), reverse=True)
    elif order == '-ordered':
        parts = sorted(parts, key=lambda p: p.quantity_ordered())

    context = {
        'parts': parts,
        'order': order,
        'num_of_cart_items': num_of_cart_items(request.user),
    }

    return render(request, 'inventory/analytics_parts.html', context)


def view_altium_parts(request):
    # This code is risk-prone, column names are listed twice.  MAKE SURE TO UPDATE BOTH!
    part_properties_static = ["Name", "Rowan Part Number", "Manufacturer", "Manufacturer part number", "Supplier 1",
                              "Supplier Part Number 1","Supplier 2", "Supplier Part Number 2", "Supplier 3",
                              "Supplier Part Number 3", "Supplier 4", "Supplier Part Number 4", "Category", "Datasheet",
                              "Description", "Library Ref", "Library Path", "Footprint Ref", "Footprint Path", "Status"]
    part_properties_dynamic = list(set(Property.objects.values_list("name", flat=True)))
    table_headers = part_properties_static + part_properties_dynamic

    parts_and_properties = []
    for part in Part.objects.filter(status=PartStatus.ACTIVE):
        supplier_name = []
        supplier_pn = []
        part_suppliers = PartSupplier.objects.filter(part=part)
        for supplier in part_suppliers:
            supplier_name.append(supplier.supplier.name)
            supplier_pn.append(supplier.supplier_part_number)
        property_values = {
            "Name": part.name,
            "Rowan Part Number": part.id,
            "Manufacturer": part.manufacturer.name if part.manufacturer is not None else None,
            "Manufacturer part number": part.manufacturer_part_number,
            "Supplier 1": supplier_name[0] if (len(supplier_name) > 0) else None,
            "Supplier Part Number 1": supplier_pn[0] if (len(supplier_pn) > 0) else None,
            "Supplier 2": supplier_name[1] if (len(supplier_name) > 1) else None,
            "Supplier Part Number 2": supplier_pn[1] if (len(supplier_pn) > 1) else None,
            "Supplier 3": supplier_name[2] if (len(supplier_name) > 2) else None,
            "Supplier Part Number 3": supplier_pn[2] if (len(supplier_pn) > 2) else None,
            "Supplier 4": supplier_name[3] if (len(supplier_name) > 3) else None,
            "Supplier Part Number 4": supplier_pn[3] if (len(supplier_pn) > 3) else None,
            "Category": part.category.name if part.category is not None else None,
            "Datasheet": part.datasheet,
            "Description": part.description,
            "Library Ref": part.altium_symbol if part.altium_symbol else None,
            "Library Path": part.altium_symbol + ".SchLib" if part.altium_symbol else None,
            "Footprint Ref": part.altium_footprint if part.altium_footprint  else None,
            "Footprint Path": part.altium_footprint + ".PcbLib" if part.altium_footprint  else None,
            "Status": part.status_label()
        }
        for part_property in PartProperty.objects.filter(part=part):
            property_values[part_property.property.name] = part_property.rendered()
        parts_and_properties.append(property_values)

    table_data = {
        "table_headers": table_headers,
        "parts_and_properties": parts_and_properties
    }
    properties_json = json.dumps(table_data)

    response = HttpResponse(properties_json, content_type="application/json")
    return response
