from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext, gettext_lazy as _


class CustomUser(AbstractUser):
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=150)
    email = models.EmailField(_('email address'))
    banner_id = models.PositiveIntegerField()
    card_uuid = models.CharField(max_length=8, null=True, blank=True)
    card_number = models.BigIntegerField(null=True, blank=True)

    def __str__(self):
        return "{} ({})".format(self.username, self.id)

    REQUIRED_FIELDS = ['first_name', 'last_name', 'email', 'banner_id']
