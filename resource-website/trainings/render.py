from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa
from django.template import engines


class Render:

    @staticmethod
    def render(path: str, params: dict):
        templatefile = open(path)
        templatestr = templatefile.read()
        django_engine = engines['django']
        template = django_engine.from_string(templatestr)
        html = template.render(params)
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
        if not pdf.err:
            return HttpResponse(response.getvalue(), content_type='application/pdf')
        else:
            return HttpResponse("Error Rendering PDF", status=400)
