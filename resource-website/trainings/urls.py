from django.urls import path

from . import views


urlpatterns = [
    path('', views.view_index, name='trainings'),
    path('certificate/<int:user_training_id>', views.view_certificate, name='certificate'),
    path('details/<int:training_id>', views.view_training_details, name='training_details'),
    path('tgdetails/', views.view_training_group_details, name='training_group_details'),

]
