from django.contrib import admin
from .models import PrintSize, PrintUse, PrintMaterial, PrintMaterialColor, PrintFilamentSpool, Printer, PrintStatus, PrintRequest

# Register your models here.



class PrintMaterialAdmin(admin.ModelAdmin):
    list_display = ('print_material', 'active')
    list_editable = ('active',)
    list_filter = ('active',)
    list_per_page = 25


class PrintMaterialColorAdmin(admin.ModelAdmin):
    list_display = ('material_color', 'active')
    list_editable = ('active',)
    list_filter = ('active',)
    list_per_page = 25


class PrintFilamentSpoolAdmin(admin.ModelAdmin):
    list_display = ('material', 'color', 'notes', 'active', 'id')
    list_editable = ('active',)
    list_filter = ('active', 'material', 'color')
    list_per_page = 25


class PrintRequestAdmin(admin.ModelAdmin):
    list_display = ('requestor', 'print_status', 'size', 'print_use', 'material', 'color', 'submitted')
    list_filter = ('print_status',)
    list_per_page = 25

admin.site.register(PrintSize)
admin.site.register(PrintUse)
admin.site.register(PrintMaterial, PrintMaterialAdmin)
admin.site.register(PrintMaterialColor, PrintMaterialColorAdmin)
admin.site.register(PrintFilamentSpool, PrintFilamentSpoolAdmin)
admin.site.register(Printer)
admin.site.register(PrintRequest, PrintRequestAdmin)