import random
from django.db import models
from django.conf import settings
from django.utils import timezone


class PrintSize(models.Model):
    size = models.CharField(max_length=128, default="")

    def __str__(self):
        return "{}".format(self.size)


class PrintUse(models.Model):
    use = models.CharField(max_length=128, default="")

    def __str__(self):
        return "{}".format(self.use)


class PrintMaterial(models.Model):
    print_material = models.CharField(max_length=128, default="")
    active = models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.print_material)


class PrintMaterialColor(models.Model):
    material_color = models.CharField(max_length=128, default="")
    active = models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.material_color)


class PrintFilamentSpool(models.Model):
    material = models.ForeignKey(PrintMaterial, on_delete=models.CASCADE)
    color = models.ForeignKey(PrintMaterialColor, on_delete=models.CASCADE)
    notes = models.TextField(default="", null=True, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return "{} {} ({})".format(self.color, self.material, self.id)


class Printer(models.Model):
    name = models.CharField(max_length=128, default="")

    def __str__(self):
        return "{}".format(self.name)


class PrintStatus(models.TextChoices):
    SUBMITTED = "SUBMITTED", "Submitted"
    PROCESSING = "PROCESSING", "Processing"
    PRINTING = "PRINTING", "Printing"
    COMPLETE = "COMPLETE", "Complete"
    PICKED_UP = "PICKED_UP", "Picked Up"
    CANCELLED = "CANCELLED", "Cancelled"
    REJECTED = "REJECTED", "Rejected"


class PrintRequest(models.Model):
    requestor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    size = models.ForeignKey(PrintSize, on_delete=models.CASCADE)
    print_use = models.ForeignKey(PrintUse, on_delete=models.CASCADE)
    material = models.ForeignKey(PrintMaterial, on_delete=models.CASCADE, blank=True, null=True)
    color = models.ForeignKey(PrintMaterialColor, on_delete=models.CASCADE, blank=True, null=True)
    spool = models.ForeignKey(PrintFilamentSpool, on_delete=models.CASCADE, blank=True, null=True)
    printer = models.ForeignKey(Printer, on_delete=models.CASCADE, blank=True, null=True)
    print_status = models.CharField(max_length=512, choices=PrintStatus.choices, default=PrintStatus.SUBMITTED)
    print_weight = models.IntegerField(blank=True, null=True)
    file = models.FileField(upload_to='uploads/printcueue/%Y/%m/%d/')
    notes = models.TextField(default="", null=True, blank=True)
    submitted = models.DateTimeField(default=timezone.now)

    def status_label(self):
        return dict(PrintStatus.choices)[self.print_status]

    def __str__(self):
        return "{} | {}".format(self.submitted, self.requestor)
