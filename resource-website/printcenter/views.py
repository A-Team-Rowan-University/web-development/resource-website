from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse
from django.utils import timezone
from django.db.models import Q

from social_django.utils import load_strategy
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.core.exceptions import PermissionDenied

from .models import PrintSize, PrintUse, PrintMaterial, PrintMaterialColor, PrintFilamentSpool, Printer, PrintStatus, \
    PrintRequest
from .forms import PrintRequestForm, PrintEditForm


@login_required(login_url='/login/google-oauth2/')
@permission_required('printcenter.add_printrequest', raise_exception=True)
def view_index(request):
    print_requests_list = PrintRequest.objects.filter(requestor=request.user).order_by('-submitted')

    if request.method == "POST":
        print_request = PrintRequest.objects.get(pk=int(request.POST['print_request_id']))
        action = request.POST['action']
        if action == 'CANCEL':
            if print_request.print_status == PrintStatus.SUBMITTED or print_request.print_status == PrintStatus.PROCESSING:
                print_request.print_status = PrintStatus.CANCELLED
                print_request.save()
        elif action == 'PICKUP':
            if print_request.print_status == PrintStatus.COMPLETE:
                print_request.print_status = PrintStatus.PICKED_UP
                print_request.save()
    context = {
        'print_requests': print_requests_list
    }

    return render(request, 'printcenter/index.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('printcenter.add_printrequest', raise_exception=True)
def view_print_request(request):
    form = PrintRequestForm(request.POST or None)

    if request.method == 'POST':
        form = PrintRequestForm(request.POST, request.FILES)
        if form.is_valid():
            printrqst = form.save(commit=False)
            printrqst.requestor = request.user
            printrqst.print_status = PrintStatus.SUBMITTED
            printrqst.save()
            return redirect('printcenter')
    else:
        form = PrintRequestForm()
        form.fields['color'].queryset = PrintMaterialColor.objects.filter(active=True)
        form.fields["material"].queryset = PrintMaterial.objects.filter(active=True)

    context = {
        'form': form,
    }

    return render(request, 'printcenter/printrequest.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('printcenter.add_printrequest', raise_exception=True)
def view_print_view(request, print_id):
    print_request = PrintRequest.objects.get(pk=print_id)
    if print_request.print_status == PrintStatus.SUBMITTED:
        progress_bar = 20
    elif print_request.print_status == PrintStatus.PROCESSING:
        progress_bar = 40
    elif print_request.print_status == PrintStatus.PRINTING:
        progress_bar = 60
    elif print_request.print_status == PrintStatus.COMPLETE:
        progress_bar = 80
    elif print_request.print_status == PrintStatus.PICKED_UP:
        progress_bar = 100
    elif print_request.print_status == PrintStatus.CANCELLED:
        progress_bar = 0
    elif print_request.print_status == PrintStatus.REJECTED:
        progress_bar = 0

    if not (print_request.requestor == request.user):
        raise PermissionDenied

    context = {
        'print_request': print_request,
        'progress_bar': progress_bar,
    }
    return render(request, 'printcenter/printview.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required(['printcenter.view_printrequest', 'printcenter.change_printrequest'], raise_exception=True)
def view_print_queue_complete(request):
    print_requests_list = PrintRequest.objects.filter(
        Q(print_status=PrintStatus.COMPLETE) |
        Q(print_status=PrintStatus.PICKED_UP)
    ).order_by('-id')
    if request.method == "POST":
        print_request = PrintRequest.objects.get(pk=int(request.POST['print_request_id']))
        action = request.POST['action']
        if action == 'PICKUP':
            if print_request.print_status == PrintStatus.COMPLETE:
                print_request.print_status = PrintStatus.PICKED_UP
                print_request.save()
    context = {
        'print_requests': print_requests_list,
        'page_title': 'Completed Prints'
    }
    return render(request, 'printcenter/printqueue.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required(['printcenter.view_printrequest', 'printcenter.change_printrequest'], raise_exception=True)
def view_print_queue(request):
    if request.method == 'POST':
        action = request.POST['action']
        printId = request.POST['printId']

        if action == 'mark_print_complete':
            printrqst = PrintRequest.objects.get(pk=printId)
            printrqst.print_status = PrintStatus.COMPLETE
            printrqst.save()
        elif action == 'mark_print_printing':
            printrqst = PrintRequest.objects.get(pk=printId)
            printrqst.print_status = PrintStatus.PRINTING
            printrqst.save()
        elif action == 'mark_printing':
            printerId = int(request.POST['printer_id'])
            printrqst = PrintRequest.objects.get(pk=printId)
            printrqst.print_status = PrintStatus.PRINTING
            printrqst.printer = Printer.objects.get(pk=printerId)
            printrqst.save()
        elif action == 'mark_uploaded':
            printerId = int(request.POST['printer_id'])
            printrqst = PrintRequest.objects.get(pk=printId)
            printrqst.print_status = PrintStatus.PROCESSING
            printrqst.printer = Printer.objects.get(pk=printerId)
            printrqst.save()

    print_requests_list = PrintRequest.objects.filter(
        Q(print_status=PrintStatus.SUBMITTED) |
        Q(print_status=PrintStatus.PROCESSING) |
        Q(print_status=PrintStatus.PRINTING)
    ).order_by('-print_status', 'id')
    printers = []
    for printer in Printer.objects.all():
        prints = PrintRequest.objects.filter(
            Q(print_status=PrintStatus.PRINTING) &
            Q(printer=printer)
        )
        queue = PrintRequest.objects.filter(
            Q(print_status=PrintStatus.PROCESSING) &
            Q(printer=printer)
        )
        if len(prints) + len(queue) > 0:
            printers.append((printer, prints, queue))

    all_printers = Printer.objects.all()
    context = {
        'print_requests': print_requests_list,
        'printers': printers,
        'all_printers': all_printers,
    }
    return render(request, 'printcenter/printqueueprinters.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required(['printcenter.view_printrequest', 'printcenter.change_printrequest'], raise_exception=True)
def view_print_edit(request, print_id):
    print_id = PrintRequest.objects.get(pk=print_id)
    form = PrintEditForm(request.POST or None, instance=print_id)
    form.fields['color'].queryset = PrintMaterialColor.objects.filter(active=True)
    form.fields["material"].queryset = PrintMaterial.objects.filter(active=True)
    form.fields["spool"].queryset = PrintFilamentSpool.objects.filter(active=True)

    print_request = print_id
    if print_request.print_status == PrintStatus.SUBMITTED:
        progress_bar = 20
    elif print_request.print_status == PrintStatus.PROCESSING:
        progress_bar = 40
    elif print_request.print_status == PrintStatus.PRINTING:
        progress_bar = 60
    elif print_request.print_status == PrintStatus.COMPLETE:
        progress_bar = 80
    elif print_request.print_status == PrintStatus.PICKED_UP:
        progress_bar = 100
    elif print_request.print_status == PrintStatus.CANCELLED:
        progress_bar = 0
    elif print_request.print_status == PrintStatus.REJECTED:
        progress_bar = 0
    if request.method == 'POST':
        form = PrintEditForm(request.POST, request.FILES, instance=print_id)
        if form.is_valid():
            printrqst = form.save(commit=False)
            printrqst.save()
            return redirect('print_queue')

    context = {
        'form': form,
        'print_request': print_request,
        'progress_bar': progress_bar,
    }

    return render(request, 'printcenter/printedit.html', context)
