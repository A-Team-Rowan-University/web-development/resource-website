from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('user_info', views.request_user_info, name='request_user_info')
]
