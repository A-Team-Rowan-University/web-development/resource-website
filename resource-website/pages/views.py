from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse

from social_django.utils import load_strategy

from .forms import UserInfoForm;


def index(request):
    return render(request, 'pages/index.html')


def request_user_info(request):
    if request.GET.get('csrfmiddlewaretoken') is not None:
        form = UserInfoForm(request.GET)
        if form.is_valid():
            request.session['banner_id'] = form.cleaned_data['banner_id']
            request.session['card_number'] = form.cleaned_data['card_number']

            return redirect(reverse('social:complete', args=('google-oauth2',)))

    else:
        initial = {}
        if request.GET.get('banner_id') is not None:
            initial['banner_id'] = request.GET.get('banner_id')

        if request.GET.get('card_number') is not None:
            initial['card_number'] = request.GET.get('card_number')

        form = UserInfoForm(initial=initial)

    context = {
        'form': form,
    }

    return render(request, 'pages/user_info.html', context)


def error_404(request, exception):
    return render(request, 'pages/404.html')


def error_403(request, exception):
    return render(request, 'pages/403.html')


def error_400(request, exception):
    return render(request, 'pages/400.html')


def error_500(request):
    return render(request, 'pages/500.html')
