from django import forms


class BannerIdField(forms.CharField):
    def validate(self, value):
        if len(value) == 9:
            try:
                num_value = int(value)
                if num_value > 900000000 and num_value < 999999999:
                    return True
                else:
                    raise forms.ValidationError('Not a valid Banner ID')
            except:
                raise forms.ValidationError('Not a valid Banner ID')
        else:
            raise forms.ValidationError('Not a valid Banner ID')

    def to_python(self, value):
        return value.replace(' ', '')
      

class RowanCardNumberField(forms.CharField):
    def validate(self, value):
        if len(value) == 16:
            try:
                num_value = int(value)
                return True
            except:
                raise forms.ValidationError('Not a valid Rowan Card Number')
        else:
            raise forms.ValidationError('Not a valid Rowan Card Number')

    def to_python(self, value):
        return value.replace(' ', '').replace('-','')


class UserInfoForm(forms.Form):
    banner_id = BannerIdField(
        label="Banner ID"
        )
    card_number = RowanCardNumberField(
        label="Rowan Card Number",
        help_text="Go to <a href=\"https://myrowancard.rowan.edu\" target=\"_blank\">https://myrowancard.rowan.edu</a> and log in. Use the \"Rowan Card "
                  "Number\" under \"your photo\" on the left side of the page."
        )
