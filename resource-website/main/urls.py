"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import logout_then_login
from markdownx import urls as markdownx

urlpatterns = [
    path('', include('pages.urls')),
    path('', include('social_django.urls', namespace='social')),
    path('logout/', logout_then_login, {'login_url': settings.LOGIN_REDIRECT_URL}, name='logout'),
    path('user_management/', include('user_management.urls')),
    path('lockout/', include('lockout.urls')),
    path('loans/', include('loans.urls')),
    path('inventory/', include('inventory.urls')),
    path('tests/', include('tests.urls')),
    path('wiki/', include('wiki.urls')),
    path('worklog/', include('worklog.urls')),
    path('printcenter/', include('printcenter.urls')),
    path('trainings/', include('trainings.urls')),
    path('admin/', admin.site.urls),
    path('markdownx/', include(markdownx))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = 'pages.views.error_404'
handler500 = 'pages.views.error_500'
handler403 = 'pages.views.error_403'
handler400 = 'pages.views.error_400'