import os
import io
import csv

import logging

import pytz

from django.core.exceptions import PermissionDenied

from django.http import HttpResponse
from django.http import FileResponse
from django.http import Http404
from django.http import QueryDict

from django.utils import timezone

from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required

from django.contrib.auth.models import Group

from user_management.models import CustomUser

from .models import Test
from .models import Session
from .models import Registration
from .models import AttemptQuestion
from .models import Answer, AttemptTraining
from .models import Category
from .models import Question
from .models import TestCategory
from .models import Attempt
from .models import TrainingTest
from trainings.models import UserTraining

from .forms import TakeTestForm
from .forms import populate_test_form_images
from .forms import CreateTestForm
from .forms import CreateSessionForm
from .forms import CreateCategoryForm
from .forms import QuestionForm
from .forms import AnswersFormSet
from .forms import CardSwipeForm
from .forms import TestCategoryFormset
from .forms import TestCategoryFormsetHelper
from .forms import TrainingTestFormset
from .forms import TrainingTestFormsetHelper

log = logging.getLogger(__name__)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.take_test', raise_exception=True)
def view_index(request):
    tests_list = Test.objects.filter(visible=True)
    context = {
        'tests': tests_list
    }
    return render(request, 'tests/index.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.take_test', raise_exception=True)
def view_test_sessions(request, test_id):
    if request.method == 'POST':
        session = Session.objects.get(pk=int(request.POST['session_id']))
        action = request.POST['action']
        if action == 'REGISTER':
            session.register(request.user)
            log.info("Registering {} for session {}".format(request.user, session))
        elif action == 'UNREGISTER':
            session.unregister(request.user)
            log.info("Unregistering {} for session {}".format(request.user, session))
        else:
            log.info("{} POSTed to session {} with unknown action {}".format(request.user, session, action))

    test = Test.objects.get(pk=test_id)

    sessions = []

    for session in test.session_set.all():
        status = session.registration_status(request.user)

        is_proctor = session.proctors.filter(pk=request.user.id).count() > 0

        sessions.append((
            session,
            status,
            session.registration_set.count(),
            session.registration_set.filter(taker=request.user).first(),
            is_proctor,
        ))

    context = {
        'test': test,
        'sessions': sessions,
    }

    return render(request, 'tests/sessions.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.proctor_session', raise_exception=True)
def view_test_session(request, session_id):
    session = Session.objects.get(pk=session_id)
    if not (session.proctors.filter(pk=request.user.id).count() > 0):
        raise PermissionDenied

    if request.method == 'POST':
        action = request.POST['action']

        if action == 'toggle_registrations_enabled':
            session.registrations_enabled = not session.registrations_enabled
            log.info("Toggling registrations to {} on session {}".format(session.registrations_enabled, session))
            session.save()
        elif action == 'toggle_opening_enabled':
            session.opening_enabled = not session.opening_enabled
            log.info("Toggling opening to {} on session {}".format(session.opening_enabled, session))
            session.save()
        elif action == 'manual_register_user':
            reguser = request.POST.get('manualRegUser', '')
            if reguser:
                session.register(CustomUser.objects.get(username=reguser))
                log.info("Registering {} for session {}".format(reguser, session))
        elif action == 'manual_unregister_user':
            unreguser = request.POST.get('manualUnRegUser', '')
            if unreguser:
                session.unregister(CustomUser.objects.get(username=unreguser))
                log.info("UnRegistering {} for session {}".format(unreguser, session))
        elif action == 'manual_add_proctor':
            proctorUsername = request.POST.get('manualAddProct', '')
            if proctorUsername:
                proctor = CustomUser.objects.get(username=proctorUsername)
                session.proctors.add(proctor)
                session.save()
                log.info("Adding {} as a proctor for session {}".format(proctorUsername, session))
        elif action == 'manual_remove_proctor':
            proctorUsername = request.POST.get('manualRemProct', '')
            if proctorUsername:
                proctor = session.proctors.get(username=proctorUsername)
                session.proctors.remove(proctor)
                log.info("Removing {} as a proctor for session {}".format(proctorUsername, session))
        else:
            log.error("Unknown action {} on session {}".format(action, session.name))

    test = session.test
    registrations = []
    for registration in session.registration_set.all().order_by('taker'):
        attempts = []
        for attempt in registration.attempt_set.all():
            grade = attempt.grade()
            attempts.append((attempt, grade * 100 if grade is not None else None))
        last_attempt = attempts.pop() if len(attempts) > 0 else None
        status = session.registration_status(registration.taker)
        registrations.append((registration, status, last_attempt, attempts))

    swipe_enabled = session.card_swipes_required

    proctors = session.proctors.all()

    context = {
        'test': test,
        'session': session,
        'registrations': registrations,
        'swipe_enabled': swipe_enabled,
        'proctors': proctors,
    }

    return render(request, 'tests/session.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.take_test', raise_exception=True)
def view_registration(request, registration_id):
    registration = Registration.objects.get(pk=registration_id)

    if registration.taker != request.user:
        raise PermissionDenied

    registration = Registration.objects.get(pk=registration_id)
    attempts = []
    for attempt in registration.attempt_set.all():
        grade = attempt.grade()
        attempts.append((attempt, grade * 100 if grade is not None else None))

    swipe_enabled = registration.session.card_swipes_required

    context = {
        'registration': registration,
        'attempts': attempts,
        'swipe_enabled': swipe_enabled,
    }

    return render(request, 'tests/registration.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.take_test', raise_exception=True)
def view_take_test(request, registration_id):
    registration = Registration.objects.get(pk=registration_id)

    if registration.taker != request.user:
        raise PermissionDenied

    # Submitting a test
    if request.method == 'POST':

        if not registration.session.registration_status(
                request.user) == 'REGISTERED_FOR_THIS_SESSION_OPENED_SUBMISSIONS_ENABLED':
            raise PermissionDenied

        attempt = Attempt.objects.filter(submitted__isnull=True).filter(registration__taker=request.user).first()
        log.info("Submitting attempt {} for registration {} with data {}".format(attempt, registration, request.POST))

        formset = TakeTestForm(request.POST)

        if attempt.submitted is None and formset.is_valid():
            formset.save()
            attempt.submitted = timezone.now()
            attempt.save()
            log.info("Attempt {} saved successfully with grade {}".format(attempt, attempt.grade()))
            # Add trainings to user if they passed the test and card swipes are disabled
            if not attempt.registration.session.card_swipes_required:
                if attempt.registration.session.registration_status(request.user) == "REGISTERED_FOR_THIS_SESSION_PASSED":
                    for trainingTest in TrainingTest.objects.filter(test=attempt.registration.session.test):
                        try:
                            existingTraining = UserTraining.objects.get(trainee=request.user, training=trainingTest.training)
                            if existingTraining.expiration < trainingTest.expiration:
                                existingTraining.expiration = trainingTest.expiration
                                existingTraining.earned = timezone.now()
                                existingTraining.save()
                            attempt_training = AttemptTraining(attempt=attempt, user_training=existingTraining)
                            attempt_training.save()
                        except UserTraining.DoesNotExist:
                            newTraining = UserTraining(
                                training=trainingTest.training,
                                trainee=request.user,
                                expiration=trainingTest.expiration,
                                earned=timezone.now())
                            newTraining.save()
                            attempt_training = AttemptTraining(attempt=attempt, user_training=newTraining)
                            attempt_training.save()

            return redirect('registrations', registration_id=registration_id)
        else:
            log.error(
                "Attempt {} submitted with data {} does not match attempt questions".format(attempt, request.POST))

            populate_test_form_images(formset)

            context = {
                'attempt': attempt,
                'formset': formset,
            }

            return render(request, 'tests/take_test.html', context)

    # Opening a test
    else:
        status = registration.session.registration_status(request.user)
        if not (status == 'REGISTERED_FOR_THIS_SESSION_NOT_OPEN_OPENING_ENABLED'
                or status == 'REGISTERED_FOR_THIS_SESSION_OPENED_SUBMISSIONS_ENABLED'):
            raise PermissionDenied

        if registration.attempt_set.filter(submitted__isnull=True).count() > 0:
            attempt = registration.attempt_set.filter(submitted__isnull=True).first()
        else:
            attempt = registration.open(request.user)

        formset = TakeTestForm(queryset=AttemptQuestion.objects.filter(attempt=attempt))

        populate_test_form_images(formset)

        context = {
            'attempt': attempt,
            'formset': formset,
        }

        return render(request, 'tests/take_test.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.create_test', raise_exception=True)
def view_create_test(request):
    if request.POST:
        form = CreateTestForm(request.POST)
        categories_formset = TestCategoryFormset(request.POST, prefix='categories')
        trainings_formset = TrainingTestFormset(request.POST, prefix='trainings')
        if form.is_valid() and categories_formset.is_valid() and trainings_formset.is_valid():
            test = Test(
                name=form.cleaned_data['name'],
                passing_score=form.cleaned_data['passing_score'] / 100,
                creator=request.user
            )

            test.save()

            test_categories = categories_formset.save(commit=False)
            for test_category in test_categories:
                test_category.test = test
                test_category.save()

            test_trainings = trainings_formset.save(commit=False)
            for test_training in test_trainings:
                test_training.test = test
                test_training.save()

            return redirect('tests')

    else:
        form = CreateTestForm()
        categories_formset = TestCategoryFormset(queryset=TestCategory.objects.none(), prefix='categories')
        trainings_formset = TrainingTestFormset(queryset=TrainingTest.objects.none(), prefix='trainings')

    categories_formset_helper = TestCategoryFormsetHelper()
    categories_formset_helper.form_tag = False
    categories_formset_helper.disable_csrf = True

    trainings_formset_helper = TrainingTestFormsetHelper()
    trainings_formset_helper.form_tag = False
    trainings_formset_helper.disable_csrf = True

    context = {
        'form': form,
        'categories_formset': categories_formset,
        'categories_formset_helper': categories_formset_helper,
        'trainings_formset': trainings_formset,
        'trainings_formset_helper': trainings_formset_helper,
    }

    return render(request, 'tests/create_test.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.take_test', raise_exception=True)
def view_test_detail(request, test_id):
    test = Test.objects.get(pk=test_id)
    passing_score = test.passing_score * 100

    categories = TestCategory.objects.filter(test=test)
    trainings = TrainingTest.objects.filter(test=test)

    context = {
        'test': test,
        'passing_score': passing_score,
        'categories': categories,
        'trainings': trainings,
    }

    return render(request, 'tests/test_detail.html', context)


# Sort the users by where or not the passed, then their last name, then their
# first name
def key_sort_users_passed(user_passed):
    user, passed = user_passed

    if passed == "FAILED":
        pass_key = 0
    elif passed == "PASSED":
        pass_key = 1
    else:
        pass_key = 2

    return str(pass_key) + user.last_name + user.first_name


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.view_test_result')
def view_test_result(request, test_id):
    test = Test.objects.get(pk=test_id)

    group_search = request.GET.get("group", "All Users")
    emails = request.GET.get("emails", "")

    group_users = CustomUser.objects.filter(groups__name=group_search)

    emails_set = set(emails.split())

    if len(emails_set) > 0:
        users = filter(lambda u: u is not None,
                       map(lambda email: group_users.filter(email=email).first(),
                           emails_set))
    else:
        users = group_users

    # All the users that took the test and whether they passed or not
    users_passed = []

    for user in users:
        passed = "NOT_TAKEN"
        for attempt in Attempt.objects.filter(registration__taker=user, registration__session__test=test):
            if attempt.passed():
                passed = "PASSED"
                break
            elif passed == "NOT_TAKEN":
                passed = "FAILED"

        users_passed.append((user, passed))

    users_passed.sort(key=key_sort_users_passed)

    if request.GET.get("export") is not None:
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{}.csv"'.format(test.name)

        writer = csv.writer(response)
        writer.writerow(['First Name', 'Last Name', 'Email', 'Banner Id', 'Passed'])

        for user, passed in users_passed:
            writer.writerow([user.first_name, user.last_name, user.email, user.banner_id, passed])

        return response

    else:

        export_query = request.GET.copy()
        export_query['export'] = "csv"

        context = {
            'test': test,
            'users_passed': users_passed,
            'group': group_search,
            'groups': Group.objects.all(),
            'emails': "\n".join(emails_set) if emails_set is not None else "",
            'query': export_query,
        }

        return render(request, 'tests/summary.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.create_test', raise_exception=True)
def view_create_session(request, test_id):
    test = Test.objects.get(pk=test_id)

    if not (test.creator == request.user):
        raise PermissionDenied

    form = CreateSessionForm(request.POST or None)

    if form.is_valid():
        session = form.save(commit=False)
        session.test = test
        session.save()
        session.save()
        session.proctors.add(request.user)
        session.save()
        return redirect('test_sessions', test_id=test.id)
    else:
        context = {
            'test': test,
            'form': form,
        }

        return render(request, 'tests/create_session.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.create_test', raise_exception=True)
def view_categories(request):
    categories = []

    for category in Category.objects.all():
        categories.append((
            category,
            category.question_set.count()
        ))

    context = {
        'categories': categories
    }

    return render(request, 'tests/categories.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.create_test', raise_exception=True)
def view_category(request, category_id):
    category = Category.objects.get(pk=category_id)

    questions = []
    for question in Question.objects.filter(category=category):
        questions.append((
            question,
            question.answer_set.all()
        ))

    context = {
        'category': category,
        'questions': questions,
    }

    return render(request, 'tests/category.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.create_test', raise_exception=True)
def view_create_category(request):
    form = CreateCategoryForm(request.POST or None)

    if form.is_valid():
        category = form.save(commit=False)
        category.creator = request.user
        category.save()
        return redirect('categories')

    context = {
        'form': form,
    }

    return render(request, 'tests/create_category.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.create_test', raise_exception=True)
def view_question(request, category_id, question_id):
    question = Question.objects.get(pk=question_id)

    if not (question.category.creator == request.user):
        raise PermissionDenied

    if request.method == 'POST':
        form = QuestionForm(request.POST, request.FILES, instance=question)
        if form.is_valid():
            form.save()

        if 'answer_set-TOTAL_FORMS' in request.POST:
            formset = AnswersFormSet(request.POST, request.FILES, instance=question)
            if formset.is_valid():
                formset.save()

    form = QuestionForm(instance=question)
    formset = AnswersFormSet(instance=question)

    context = {
        'category': question.category,
        'form': form,
        'formset': formset,
    }

    return render(request, 'tests/question.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.create_test', raise_exception=True)
def view_create_question(request, category_id):
    if request.user != Category.objects.get(pk=category_id).creator:
        raise PermissionDenied

    if request.method == 'POST':
        form = QuestionForm(request.POST, request.FILES)

        if form.is_valid():
            question = form.save(commit=False)
            question.category_id = category_id
            question.save()
            question_id = question.id
            return redirect('question', category_id=category_id, question_id=question_id)
    else:
        form = QuestionForm()

    context = {
        'category_id': category_id,
        'form': form,
    }

    return render(request, 'tests/create_question.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.proctor_test', raise_exception=True)
def view_card_swipe_in(request, session_id):
    session = Session.objects.get(pk=session_id)

    swiped_user = None
    error = None

    if request.method == "POST":
        form = CardSwipeForm(request.POST)
        if form.is_valid():
            swiped_user = CustomUser.objects.filter(card_number=form.cleaned_data['card_number']).first()
            log.info("Swiping in user {} for session {} with data {}".format(swiped_user, session, request.POST))
            if swiped_user is None:
                error = "NOT_FOUND"
            else:
                swiped_user.card_uuid = form.cleaned_data['card_uid']
                swiped_user.save()
                registration = Registration.objects.filter(taker=swiped_user, session_id=session.id).first()
                if registration is None:
                    error = "NOT_REGISTERED"
                else:
                    registration.swiped_in = timezone.now()
                    registration.save()
        else:
            error = "INVALID_FORM"
            log.error("Card swipe with data {} is an invalid form".fomat(request.POST))

    form = CardSwipeForm()
    if error is None and swiped_user is not None:
        audio = "swipe-in.wav"
    elif error is not None:
        audio = "swipe-fail.wav"
    else:
        audio = None

    swipe_enabled = session.card_swipes_required

    context = {
        "swiped_user": swiped_user,
        "error": error,
        "session": session,
        "title": "Swipe in for {}".format(session.name),
        "form": form,
        "audio": audio,
        'swipe_enabled': swipe_enabled,
    }

    return render(request, "tests/card_swipe.html", context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('tests.proctor_test', raise_exception=True)
def view_card_swipe_out(request, session_id):
    session = Session.objects.get(pk=session_id)

    swiped_user = None
    error = None

    if request.method == "POST":
        form = CardSwipeForm(request.POST)
        if form.is_valid():
            swiped_user = CustomUser.objects.filter(card_number=form.cleaned_data['card_number']).first()
            log.info("Swiping out user {} for session {} with data {}".format(swiped_user, session, request.POST))
            # Add trainings to user upon swipe out
            # This code is duplicated on test submit for non-swipe tests
            # Its not exactly the same but is very similar
            if session.registration_status(swiped_user) == "REGISTERED_FOR_THIS_SESSION_PASSED":
                for trainingTest in TrainingTest.objects.filter(test=session.test):
                    try:
                        existingTraining = UserTraining.objects.get(trainee=swiped_user, training=trainingTest.training)
                        if existingTraining.expiration < trainingTest.expiration:
                            existingTraining.expiration = trainingTest.expiration
                            existingTraining.earned = timezone.now()
                            existingTraining.save()
                    except UserTraining.DoesNotExist:
                        newTraining = UserTraining(
                            training=trainingTest.training,
                            trainee=swiped_user,
                            expiration=trainingTest.expiration,
                            earned=timezone.now())
                        newTraining.save()

            if swiped_user is None:
                error = "NOT_FOUND"
            else:
                swiped_user.card_uuid = form.cleaned_data['card_uid']
                swiped_user.save()
                registration = Registration.objects.filter(taker=swiped_user, session_id=session.id).first()
                if registration is None:
                    error = "NOT_REGISTERED"
                else:
                    registration.swiped_out = timezone.now()
                    registration.save()
        else:
            error = "INVALID_FORM"
            log.error("Card swipe with data {} is an invalid form".format(request.POST))

    form = CardSwipeForm()
    if error is None and swiped_user is not None:
        audio = "swipe-out.wav"
    elif error is not None:
        audio = "swipe-fail.wav"
    else:
        audio = None

    swipe_enabled = session.card_swipes_required

    context = {
        "swiped_user": swiped_user,
        "error": error,
        "session": session,
        "title": "Swipe out for {}".format(session.name),
        "form": form,
        "audio": audio,
        'swipe_enabled': swipe_enabled,
    }

    return render(request, "tests/card_swipe.html", context)
