from django.contrib import admin

from .models import Test, Category, Question, Answer, Session, Registration, AttemptQuestion, TestCategory, Attempt, \
    TrainingTest, AttemptTraining


# Register your models here.

# Each table in the testing app has a automatically generated UI by using admin.site.register(thing)
# By defining a ModelAdmin, we are able to customize what is displayed in the overall layout view ot make the admin panel more useful


class AttemptQuestionInline(admin.TabularInline):
    model = AttemptQuestion
    extra = 0


class AttemptAdmin(admin.ModelAdmin):
    def taker(self, obj):
        return obj.registration.taker
    
    def session(self, obj):
        return obj.registration.session
    
    list_display = ('registration', 'opened', 'submitted', 'taker', 'session', 'passed')
    list_per_page = 25
    inlines = [AttemptQuestionInline]


class AttemptTrainingAdmin(admin.ModelAdmin):
    model=AttemptTraining


class RegistrationAdmin(admin.ModelAdmin):
    list_display = ('session', 'taker', 'registered', 'swiped_in', 'swiped_out')
    list_display_links = ('session', 'taker')
    list_filter = ('session',)
    list_per_page = 25


# This allows answers to be added from the create question dialog in DjangoAdmin
class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 4


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'category', 'image')
    list_filter = ('category',)
    list_per_page = 25
    inlines = [AnswerInline]


class SessionAdmin(admin.ModelAdmin):
    list_display = ('name', 'test', 'max_registrations', 'attempts', 'registrations_enabled', 'opening_enabled', 'card_swipes_required')
    list_editable = ('registrations_enabled', 'opening_enabled')
    list_filter = ('test',)
    list_per_page = 25


class AttemptQuestionAdmin(admin.ModelAdmin):
    list_per_page = 25


class TestCategoryAdmin(admin.ModelAdmin):
    list_display = ('category', 'test', 'number_of_questions')
    list_filter = ('test',)
    list_per_page = 25


class TestCategoryInline(admin.TabularInline):
    model = TestCategory
    extra = 4


class TestTrainingInline(admin.TabularInline):
    model = TrainingTest
    extra = 0


class TestAdmin(admin.ModelAdmin):
    list_display = ('name', 'passing_score', 'visible', 'creator')
    list_editable = ('visible',)
    list_per_page = 25
    inlines = [TestCategoryInline, TestTrainingInline]


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'creator')
    list_filter = ('creator',)
    list_per_page = 25


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('question', 'text', 'correct', 'image')
    list_per_page = 25


# Register all classes with their ModelAdmins so they display in the admin panel
admin.site.register(Attempt, AttemptAdmin)
admin.site.register(AttemptTraining, AttemptTrainingAdmin)
admin.site.register(Registration, RegistrationAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Session, SessionAdmin)
admin.site.register(Test, TestAdmin)
admin.site.register(Category, CategoryAdmin)


# These have been disabled because they are now using inlines within the objects that they are related to.
# If needed, they can be re-enabled by un-commenting below
#admin.site.register(Answer, AnswerAdmin)
#admin.site.register(AttemptQuestion, AttemptQuestionAdmin)
#admin.site.register(TestCategory, TestCategoryAdmin)