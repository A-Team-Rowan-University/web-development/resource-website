import argparse
import csv
import math

from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from tests.models import Test, Session, Registration, Attempt, Question, AttemptQuestion, Answer

from user_management.models import CustomUser


class Command(BaseCommand):
    help = (
        "Imports sessions and registrations from a csv file in the format \n"
        "test name| test session name | max registrations | registrations enabled | opening enabled |"
        " submissions enabled | email | registered date | opened date | submitted date | score\n"
    )

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs=1, type=argparse.FileType('rt'))
        parser.add_argument('dummy_question_id', nargs=1, type=int)

    def handle(self, *args, **options):
        file = options['csv_file'][0]
        reader = csv.reader(file)

        dummy_question = Question.objects.get(pk=options['dummy_question_id'][0])
        self.stdout.write("Using dummy question: {}".format(dummy_question.question_text))

        dummy_correct = dummy_question.answer_set.filter(correct=True).first()
        dummy_incorrect = dummy_question.answer_set.filter(correct=False).first()

        test = None
        test_session = None
        total_questions = None

        for i, row in enumerate(reader):
            self.stdout.write("Importing row {}".format(i))
            test_name = row[0]
            if test is None or test.name != test_name:
                test = Test.objects.filter(name=test_name).first()
                if test is None:
                    test = Test(name=test_name, passing_score=0.8)
                    test.save()

            test_session_name = row[1]
            if test_session is None or test_session.name != test_session_name:
                test_session = Session.objects.filter(name=test_session_name).first()
                if test_session is None:
                    test_session_max_registrations = row[2]
                    test_session_registrations_enabled = row[3]
                    test_session_opening_enabled = row[4]
                    test_session_submissions_enabled = row[5]
                    test_session = Session(
                        name=test_session_name,
                        max_registrations=test_session_max_registrations,
                        registrations_enabled=test_session_registrations_enabled,
                        opening_enabled=test_session_opening_enabled,
                        submissions_enabled=test_session_submissions_enabled,
                        test=test,
                    )
                    test_session.save()

                    total_questions = 0
                    for test_category in test.testcategory_set.all():
                        total_questions += test_category.number_of_questions

            email = row[6]
            user = CustomUser.objects.filter(email=email).first()
            if user is None:
                self.stderr.write("User with email not found: {}".format(email))
                continue

            registered_date = timezone.make_aware(datetime.fromisoformat(row[7]))
            opened_date = row[8]
            submitted_date = row[9]

            if len(opened_date) > 0 and len(submitted_date) == 0:
                opened_date = timezone.make_aware(datetime.fromisoformat(opened_date))

                registration = Registration(session=test_session, registered=registered_date, swiped_in=opened_date, taker=user)
                registration.save()

                attempt = Attempt(registration=registration, opened=opened_date)
                attempt.save()

            elif len(opened_date) > 0 and len(submitted_date) > 0:
                opened_date = timezone.make_aware(datetime.fromisoformat(opened_date))
                submitted_date = timezone.make_aware(datetime.fromisoformat(submitted_date))

                registration = Registration(session=test_session, registered=registered_date, swiped_in=opened_date, swiped_out=submitted_date, taker=user)
                registration.save()

                attempt = Attempt(registration=registration, opened=opened_date, submitted=submitted_date)
                attempt.save()

                score = float(row[10])

                # Round down the scores to match the number of questions in the test
                # in case the math doesn't work nicely
                correct_questions = math.floor(total_questions * score)

                # Make sure we don't fail anyone that actually passed
                # Accounts for floating point errors or if the total number of questions changed
                if (score >= 0.8) and (correct_questions / total_questions < 0.8):
                    correct_questions = math.ceil(total_questions * 0.8)
                    self.stdout.write("Passing mismatch. Score: {}, correct questions: {}".format(score, correct_questions))

                # Make sure we don't pass anyone that actually failed
                if (score < 0.8) and (correct_questions / total_questions >= 0.8):
                    correct_questions = math.floor(total_questions * 0.75)
                    self.stdout.write("Failing mismatch. Score: {}, correct questions: {}".format(score, correct_questions))

                for i in range(0, total_questions):
                    if i < correct_questions:
                        attempt_question = AttemptQuestion(attempt=attempt, question=dummy_question, selected_answer=dummy_correct)
                        attempt_question.save()
                    else:
                        attempt_question = AttemptQuestion(attempt=attempt, question=dummy_question, selected_answer=dummy_incorrect)
                        attempt_question.save()

