# Generated by Django 3.0.2 on 2020-06-25 02:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tests', '0033_auto_20200624_2155'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='test',
            options={'permissions': (('take_test', 'Can take test'), ('view_test_result', 'View test result'))},
        ),
    ]
