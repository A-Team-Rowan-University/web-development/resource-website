from django.db import models
from markdownx.models import  MarkdownxField
from markdownx.utils import markdownify


class PageStatus(models.TextChoices):
    LISTED = "LISTED", "Listed"
    UNLISTED = "UNLISTED", "Unlisted"
    DRAFT = "DRAFT", "Draft"


class Page(models.Model):
    name = models.CharField(max_length=512, null=False, blank=False)
    slug = models.SlugField(max_length=512, null=False, blank=False)
    status = models.CharField(
        max_length=64,
        choices=PageStatus.choices,
        default=PageStatus.DRAFT,
    )
    markdown = MarkdownxField()

    def __str__(self):
        return "{} ({})".format(self.name, self.id)

    @property
    def formatted_markdown(self):
        return markdownify(self.markdown)
