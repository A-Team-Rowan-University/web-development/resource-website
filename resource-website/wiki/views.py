from django.http import HttpResponse
from django.shortcuts import render

from .models import Page, PageStatus

def index(request):
    pages = Page.objects.filter(status="LISTED")

    context = {
        'pages': pages,
    }

    return render(request, 'wiki/index.html', context)


def page(request, slug):
    page = Page.objects.get(slug=slug)

    if not page.status == "DRAFT":
        context = {
            'page': page,
        }
        return render(request, 'wiki/page.html', context)
    else:
        return index(request)