from django.contrib import admin

from markdownx.models import MarkdownxField
from markdownx.admin import MarkdownxModelAdmin

from .models import Page


class PageAdmin(MarkdownxModelAdmin):
    prepopulated_fields = {
        'slug': ('name', )
    }


admin.site.register(Page, PageAdmin)