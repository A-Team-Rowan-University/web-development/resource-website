from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse
from django.utils import timezone

from social_django.utils import load_strategy
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.core.exceptions import PermissionDenied

from .models import WorkLocation
from .models import WorkEntry
from .forms import WorkStartForm, WorkEndForm, WorkEditForm, CreateWorkLocationForm


@login_required(login_url='/login/google-oauth2/')
@permission_required('worklog.add_workentry', raise_exception=True)
def view_index(request):
    workentries_list = WorkEntry.objects.filter(worker=request.user).order_by('-work_start')
    
    workentries_hours_list = []
    for entry in workentries_list:
        workentries_hours_list.append((
            entry,
            round((entry.work_end - entry.work_start).seconds / 3600, 1) if entry.work_end is not None else None,
        ))
    
    context = {
        'workentries': workentries_hours_list
    }
    return render(request, 'worklog/index.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('worklog.add_workentry', raise_exception=True)
def view_enter_work(request):
    form = WorkStartForm(request.POST or None)

    if form.is_valid():
        WorkEntry = form.save(commit=False)
        WorkEntry.worker = request.user
        WorkEntry.save()
        return redirect('worklog')

    context = {
        'form': form,
    }
    
    return render(request, 'worklog/enterwork.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('worklog.add_workentry', raise_exception=True)
def view_finish_work(request, entry_id):
    entry = WorkEntry.objects.get(pk = entry_id)
    form = WorkEndForm(initial={'work_end': timezone.now()}, instance=entry)

    if not (entry.worker == request.user):
        raise PermissionDenied
    
    if request.method=="POST":
        form = WorkEndForm(request.POST, instance=entry)
        if form.is_valid():
            form.save()
            return redirect('worklog')


    context = {
        'form': form,
    }
    
    return render(request, 'worklog/finishwork.html', context)

@login_required(login_url='/login/google-oauth2/')
@permission_required('worklog.add_workentry', raise_exception=True)
def view_edit_work(request, entry_id):
    entry = WorkEntry.objects.get(pk=entry_id)
    form = WorkEditForm(initial={'work_end': timezone.now()}, instance=entry)

    if not (entry.worker == request.user):
        raise PermissionDenied

    if request.method == "POST":
        form = WorkEditForm(request.POST, instance=entry)
        if form.is_valid():
            form.save()
            return redirect('worklog')

    context = {
        'form': form,
    }

    return render(request, 'worklog/editwork.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('worklog.view_worklocation', raise_exception=True)
def view_locations(request):
    worklocations_list = WorkLocation.objects.all()
    context = {
        'worklocations': worklocations_list
    }
    return render(request, 'worklog/locations.html', context)


@login_required(login_url='/login/google-oauth2/')
@permission_required('worklog.add_worklocation', raise_exception=True)
def view_create_location(request):
    form = CreateWorkLocationForm(request.POST or None)

    if form.is_valid():
        WorkLocation = form.save(commit=False)
        WorkLocation.save()
        return redirect('locations')

    context = {
        'form': form,
    }
    
    return render(request, 'worklog/createlocation.html', context)