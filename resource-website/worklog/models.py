import random
from django.db import models
from django.conf import settings
from django.utils import timezone
#from django.contrib.auth.models import User

# Create your models here.

class WorkLocation(models.Model):
    name = models.CharField(max_length=128, default="")
    
    def __str__(self):
        return "{}".format(self.name)
        #return "{} ({})".format(self.name, self.id)

class WorkEntry(models.Model):
    worker = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    work_location = models.ForeignKey(WorkLocation, on_delete=models.CASCADE)
    work_description = models.TextField(default="")
    work_start = models.DateTimeField(default = timezone.now)
    work_end = models.DateTimeField(null=True)