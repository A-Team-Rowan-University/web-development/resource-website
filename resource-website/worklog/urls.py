from django.urls import path

from . import views

urlpatterns = [
    path('', views.view_index, name='worklog'),
    path('enterwork', views.view_enter_work, name='enter_work'),
    path('finishwork/<int:entry_id>', views.view_finish_work, name='finish_work'),
    path('editwork/<int:entry_id>', views.view_edit_work, name='edit_work'),
    path('createlocation', views.view_create_location, name='create_location'),
    path('locations', views.view_locations, name='locations'),
]
