import requests
import pyodbc
import os
print("Starting")
web_server = os.getenv('WEB_HOST')
sql_server = os.getenv('MSSQL_HOST')
database = os.getenv('MSSQL_DB')
username = os.getenv('MSSQL_USER')
password = os.getenv('MSSQL_PASSWORD')

url = 'http://%s/inventory/altium_parts' % web_server
print("Connecting to: " + url)
response = requests.get(url)
responseJson = response.json()
print(response)

connection_string = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+sql_server+';DATABASE='+database+';UID='+username+';PWD='+ password
print("Connecting to MSSQL server using string: " + connection_string)
connection = pyodbc.connect(connection_string, autocommit=True)
cursor = connection.cursor()
print("Connected!")

# Create altium database if it does not already exist
cursor.execute("""
 IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'altiumdb')
   BEGIN
     CREATE DATABASE altiumdb
   END
 """)

# Switch context to the altiumdb database
cursor.execute("USE altiumdb")
try:
    cursor.execute("CREATE SCHEMA altiumdb")
except:
    print("Schema already exists")

# Drop table under new name if it exists
try:
    cursor.execute("DROP TABLE altiumdb.altiumdb_new")
except:
    print("new table does not exist")

# Create table
table_string = """
create table altiumdb.altiumdb_new
(
"""
for header in responseJson["table_headers"]:
    table_string += "\"" + header + "\" NVARCHAR(250),\n"
table_string += ")"
cursor.execute(table_string)

# Put parts into new table
for part in responseJson["parts_and_properties"]:
    parts_headers_string = "INSERT INTO altiumdb.altiumdb.altiumdb_new ("
    parts_data_string = "VALUES ("
    for propertyName, propertyValue in part.items():
        if propertyValue is not None:
            parts_headers_string += "\"" + str(propertyName) + "\","
            parts_data_string += "N\'" + str(propertyValue) + "\',"
    # Remove extra comma to stop server from complaining
    parts_headers_string = parts_headers_string[:-1]
    parts_data_string = parts_data_string[:-1]
    parts_headers_string += ")\n"
    parts_data_string += ")\n"
    queryString = parts_headers_string + parts_data_string
    cursor.execute(queryString)

# DROP Current table, replace with altiumdb_new table
try:
    cursor.execute("DROP TABLE altiumdb.altiumdb")
except:
    pass
cursor.execute("EXEC sp_rename 'altiumdb.altiumdb_new', 'altiumdb';")

# Add altiumdesigner user to db table
try:
    cursor.execute("create login altiumdesigner with password = '', CHECK_POLICY = OFF;")
except:
    pass
try:
    cursor.execute("create user altiumdesigner for login altiumdesigner")
except:
    pass
try:
    cursor.execute("grant select on altiumdb.altiumdb to altiumdesigner")
except:
    pass
print("Done")
