> Use this template to tell us about something new you would like to see
> implemented. We will then ask you questions, and eventually come up with a
> design document. This will help make sure that both you and us are happy with
> the result.
>
> Following these templates will make it much easier for us to help you.
>
> After you have filled out this template, remove all the lines starting with `>`.

# End Goal
> Here describe what problem you are trying to address with this feature. For example:
>
> "Students and faculty need to be able to submit online orders for parts in
> stock, and pick them up later."
>
> Try to keep this somewhat generic. Avoid talking about specific user interface or
> database elements, and instead stick to the problem domain.

# Possible implementation
> Here, you can describe any possible implementations you may be thought up.
> This can include workflows, user interfaces, database layouts, etc.

# Alternatives
> Have you tried anything to solve this problem before? Are there other,
> existing ways to do what you want? Is it possible that this software can do it,
> but not quite in the way that you need?
>
> Here, try to describe other ways to filling your goal, and tell us why
> you don't think they would work.